import React, { useEffect, useState } from "react";
import Header from "../Header";
import axios from "axios";

export default function Question1() {
  const [value, setValue] = useState("");
  const [checked1, setChecked1] = useState(false);
  const [checked2, setChecked2] = useState(false);
  const [checked3, setChecked3] = useState(false);
  const [disable, setDisable] = useState(false);
  const [btnDisable, setBtnDisable] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const response = await axios.get("http://localhost:8000/answers/3");
    if (response.data.answer !== null) {
      if (response.data.answer === "merah") {
        setChecked1(true);
      } else if (response.data.answer === "kuning") {
        setChecked2(true);
      } else if (response.data.answer === "orange") {
        setChecked3(true);
      }
      setDisable(true);
      setBtnDisable(true);
    }
  };

  const submit = async e => {
    if (disable === false) {
      await axios.put("http://localhost:8000/answers/3", {
        answer: value,
      });
    }
  };
  const handleChange = e => {
    if (e.target.checked === true) {
      setValue(e.target.value);
      setBtnDisable(true);
    }
  };

  const handleClick1 = () => {
    setChecked1(true);
    setChecked2(false);
    setChecked3(false);
  };
  const handleClick2 = () => {
    setChecked2(true);
    setChecked1(false);
    setChecked3(false);
  };
  const handleClick3 = () => {
    setChecked1(false);
    setChecked2(false);
    setChecked3(true);
  };

  return (
    <div className='text-center my-5'>
      <Header soalId={"Ketiga"} />
      <div className='mt-5'>
        <p>Apa warna dari buah pisang ?</p>
        <form onSubmit={submit} action='/question4'>
          <div className='my-2'>
            Merah <input disabled={disable} type='radio' name='answer' onClick={() => handleClick1()} checked={checked1} onChange={e => handleChange(e)} value={"merah"} />
            <br />
            Kuning <input disabled={disable} type='radio' name='answer' onClick={() => handleClick2()} checked={checked2} onChange={e => handleChange(e)} value={"kuning"} />
            <br />
            Orange
            <input disabled={disable} type='radio' name='answer' onClick={() => handleClick3()} checked={checked3} onChange={e => handleChange(e)} value={"orange"} />
            <br />
          </div>
          <a href='/question2' className='btn btn-primary px-5 py-2 mx-3'>
            Prev
          </a>
          <input type='submit' value='Next' className={`${btnDisable ? "" : "disabled"} btn btn-primary px-5 py-2`} />
        </form>
      </div>
    </div>
  );
}
