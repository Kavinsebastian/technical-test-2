import { useEffect, useState } from "react";
import axios from "axios";
export default function Home() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const response = await axios
      .get("http://localhost:8000/answers")
      .then(result => {
        setData(result.data);
      })
      .catch(err => console.log(err));
  };

  console.log(data.length);

  return (
    <div className='container text-center py-5'>
        <h1>selamat anda telah mengerjakan dengan selesai</h1>
        <br />
        <br />
        {
          data.map((element, i) => {
          return <h3 key={i}>soal ke {i + 1} anda menjawab : {element.answer}</h3>;
        })}
        <p className="mt-5 my-5">Terima Kasih</p>
      </div>
  );
}
