import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Question1 from './Components/Questions/Question1';
import Question2 from './Components/Questions/Question2'
import Question3 from './Components/Questions/Question3'
import Question4 from './Components/Questions/Question4'
import Home from './Components/Home';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App />}/>
        <Route path='/question1' element={<Question1/>}/>
        <Route path='/question2' element={<Question2/>}/>
        <Route path='/question3' element={<Question3/>}/>
        <Route path='/question4' element={<Question4/>}/>
        <Route path='/home' element={<Home/>}/>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
