# Technical test ke 2

### Deskripsi

ini adalah project react dimana kita dapat mengambil value dari inputan user, saya membuat project ini dengan
soal-soal seputar warna dari buah-buahan(yang mudah-mudah aja hehe..). dalam pembuatan project ini
saya sedikit mempercantik tampilan dengan menggunakan css framework dari reactstrap yang khusus untuk react.
untuk menyimpan dari inputan user saya menggunakan dummy API dari [json-server](https://www.npmjs.com/package/json-server)
tanpa membuat database.

untuk menjalankan project ini saya sertakan step by step nya.

### Step By Step
hal yang paling utama mungkin anda sudah mengetahui untuk clone repository. 
1. setelah anda selesai cloning dari directory anda, anda dapat membuka nya menggunakan code editor pilihan anda.
2. install npm dengan mengetik di terminal
* $ npm install. 
3. jalankan terlebih dahulu json-server dengan mengetik di terminal
* $ npx json-server --watch db.json --port 8000 (apa itu db.json ? db.json adalah file yang saya simpan untuk menyimpan value/inputan dari user). 
* anda dapat mengecek nya di [http://localhost:8000/answers](http://localhost:8000/answers). 
4. jalan kan project react dengan mengetik di terminal
* $ npm start. 
* anda dapat melihat tampilan dari project ini di [http://localhost:3000](http://localhost:3000). 


sekarang anda dapat melihat hasil dari project saya terima kasih :)




